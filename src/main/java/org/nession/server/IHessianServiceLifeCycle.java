
package org.nession.server;

/**
 * Hessian服务生命周期控制
 * 
 * @author 君枫
 * @version 1.0
 * @since 2013年12月22日 下午3:50:28
 */
public interface IHessianServiceLifeCycle
{

	/**
	 * 服务开启
	 */
	public void startUp();

	/**
	 * 服务停止
	 */
	public void shutDown();
}
