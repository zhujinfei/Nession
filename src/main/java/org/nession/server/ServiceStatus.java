
package org.nession.server;

/**
 * 服务状态
 * 
 * @author 君枫
 * @version 1.0
 * @since 2013年12月24日 下午3:10:37
 */
public enum ServiceStatus
{
	Starting, Started, Stopping, Stopped, NotStarted;
}
