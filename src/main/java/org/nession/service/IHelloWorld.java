
package org.nession.service;

import java.io.File;
import java.util.Map;

/**
 * @author wangyj9
 * @version 1.0
 * @since 2013年12月22日 下午7:33:54
 */
public interface IHelloWorld
{

	String hello(String name);

	boolean sendBigData(Map map);

	Map<String, Object> reciveBigData(String id);

	boolean upload(File file);

	String introduce(Person p);

	void sendMsg(String msg);
}
